<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\VacunaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [VacunaController::class, 'index'])->name('home');

Route::get('vacunas/{vacuna}', [VacunaController::class, 'show'])->name('vacuna.show');

Route::get('api/vacunas/{idPaciente}', [RestController::class, 'pacientes']);

Route::post ('api/vacunas/crear',[RestController::class, 'store']);

