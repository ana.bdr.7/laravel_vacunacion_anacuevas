<?php

namespace Database\Factories;

use App\Models\Paciente;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class PacienteFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Paciente::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition(){
        return[
            'nombre' =>$this->faker->name,
            'slug' => Str::slug('nombre'),
            'vacunado' =>$this->faker->boolean,
            'fechaVacuna' => $this->faker->date,
            'grupo_id' =>random_int(\DB::table('grupos')->min('id'), \DB::table('grupos')->max('id'))
        ];
    }
     

}
