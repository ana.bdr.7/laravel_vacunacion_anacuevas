<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;
use Database\Seeders\VacunaSeeder;
use Database\Seeders\GrupoSeeder;
use App\Models\Paciente;



class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        

        DB::table('grupos')->delete();
        $this->call(GrupoSeeder::class);
        DB::table('vacunas')->delete();
        $this->call(VacunaSeeder::class);
        Paciente::factory(25)->create();

       
    }
}

