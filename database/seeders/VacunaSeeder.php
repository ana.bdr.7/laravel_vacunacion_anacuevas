<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Vacuna;
use App\Models\Grupo;


class VacunaSeeder extends Seeder
{
    private $vacunas = ['Pfizer-BioNTech', 'Moderna', 'Oxford-AstraZeneca', 
        'Sputnik V', 'Johnson & Johnson', 'Novavax', 'Sinovac'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach($this->vacunas as $nombreVacuna)
        {
            $vacuna = new Vacuna();
            $vacuna->nombre = $nombreVacuna;
            $vacuna->slug = Str::slug($nombreVacuna);
            $vacuna->save();
        

           
        }
    }
}
