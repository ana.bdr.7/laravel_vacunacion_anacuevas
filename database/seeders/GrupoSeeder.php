<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
Use Illuminate\Support\Str;
use App\Models\Grupo;



class GrupoSeeder extends Seeder
{
    private $grupos = ['Residencias mayores', 'Sanitarios', 
            'Mayores de 80', 'Esenciales', 'Mayores de 70', 'Mayores de 55', 'Resto de población'];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $prioridad = 40;
        foreach(array_reverse($this->grupos) as $nombreGrupo)
        {
            $grupo = new Grupo();
            $grupo->nombre = $nombreGrupo;
            $grupo->slug = Str::slug($nombreGrupo);
            $grupo->prioridad = $prioridad;
            $grupo->save();
            $prioridad+=10;
        }
    }
}

