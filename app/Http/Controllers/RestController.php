<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RestController extends Controller
{
    public function store(Request $request){

        $datos = $request->except('_token');
        $datos['nombre'] = $request->nombre;
        

        try{
            $producto = Vacunas::create($datos);
            return response()->json(['mensaje' => 'Vacuna insertada correctamente']);
        }catch(Illuminate\Database\QueryException $ex){            
            return response()->json(['mensaje'=>'Fallo al subir al subir la vacuna']);
        

        }   
    }

    public function pacientes($id_paciente){

        $vacunas = DB::table('vacunas')
            ->join('pacientes', 'grupo.id', '=', '') 
            ->where('pacientes.id',$id_paciente)->first();

    }
}
