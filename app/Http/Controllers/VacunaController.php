<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Vacuna;

class VacunaController extends Controller
{
    public function index(){
        $vacunas = Vacuna::all();

        return view('inicio', ['vacunas' => $vacunas]);

    }

    public function show($vacuna){

        $vacuna = Vacuna::find($vacuna);

        return view('vacunas.show', ['vacuna' => $vacuna]);

    }
}
