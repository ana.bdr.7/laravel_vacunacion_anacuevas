<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Paciente;
use App\Models\Vacuna;

class Grupo extends Model
{
    protected $guarded = [];

    public function pacientes(){
        return $this->hasMany(Paciente::class);
    }

    public function vacunas(){
        return $this->belongsToMany(Vacuna::class, 'gurpo_vacunas','grupo_id','vacuna_id');
    }
    
        
   
}
