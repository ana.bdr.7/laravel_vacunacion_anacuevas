<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vacuna extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function grupos(){
        return $this->belongsToMany(Grupo::class, 'gurpo_vacunas','grupo_id','vacuna_id');
    }
}
