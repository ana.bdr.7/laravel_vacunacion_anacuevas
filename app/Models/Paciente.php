<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Vacuna;

class Paciente extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function vacuna(){
        return $this->belongsTo(Vacuna::class);
    }

        
}
