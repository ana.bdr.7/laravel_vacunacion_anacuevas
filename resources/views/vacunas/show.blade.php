@extends('layouts.master') 
 
@section('titulo')
  Vacunas
@endsection 
 
@section('contenido')
  

<div class="col-sm-12 col-md-4">        
        <div class="card" style="width: 18rem;">                    
                <div class="card-body">
                    <h5 class="card-title">{{$vacuna->nombre}}</h5>
                    <h5 class="card-title">Pacientes no vacunados</h5>
                    <ul>
                    <li class="card-text">{{$vacuna->grupos}}</li>
                    </ul>                            
                </div>
            </div>   
    </div>
 
</div>
@endsection 