@extends('layouts.master') 
 
@section('titulo')
  Vacunas
@endsection 
 
@section('contenido')
  
@foreach($vacunas as $vacuna => $vac)
<div class="col-sm-12 col-md-4">        
                <div class="card" style="width: 18rem;">                    
                        <div class="card-body">
                            <h5 class="card-title">{{$vac->nombre}}</h5>
                            <h5 class="card-title">Posibles grupos de vacunacion</h5>
                            <ul>
                            <li class="card-text">{{$vac->grupos}}</li>
                            </ul>
                            <a href="{{route('vacuna.show', $vac->slug)}}">Mas Info</a>
                        </div>
                 </div>   
            </div>
 
  @endforeach
</div>
@endsection 
